﻿using Gtd3Tier.DataAccessLayer.Model;
using System;
using System.Collections.Generic;

namespace Gtd3Tier.BusinessLayer.Services
{
    public interface ITaskService
	{
		GtdTask AddTask(string userName, string name, string description, DateTime? startDate, DateTime? endDate);

		GtdTask UpdateTask(int taskId, string newName, string newDescription);

		string MoveTaskToState(int taskId, Direction direction);

		void ChangeTaskToState(int taskId, int stateId);

		GtdTask GetTask(int taskId);

		IEnumerable<GtdTask> GetAllTasks();

		IEnumerable<GtdTask> GetTaskForUser(string userName);

		int DeleteTask(int taskId);
	}

	public enum Direction
	{
		Forwards = 1,
		Backwards = 0
	};
}
