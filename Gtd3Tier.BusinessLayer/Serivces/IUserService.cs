﻿using Gtd3Tier.DataAccessLayer.Model;

namespace Gtd3Tier.BusinessLayer.Services
{
    public interface IUserService
	{
		bool AuthenticateUser(string userName, string password);
		bool RegisterUser(string userName, string password, string confirmPassword);
		User GetUser(string userName);
	}
}
