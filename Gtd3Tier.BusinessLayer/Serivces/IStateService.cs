﻿using Gtd3Tier.DataAccessLayer.Model;
using System.Collections.Generic;

namespace Gtd3Tier.BusinessLayer.Services
{
    public interface IStateService
	{
		KanbanState GetState(int stateId);

		IEnumerable<KanbanState> GetAllStates();
	}
}
