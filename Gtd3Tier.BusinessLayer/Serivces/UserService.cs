﻿using Gtd.Infrastructure.Repositories;
using Gtd3Tier.DataAccessLayer.Context;
using Gtd3Tier.DataAccessLayer.Model;
using System;
using System.Linq;
using System.Security.Cryptography;

namespace Gtd3Tier.BusinessLayer.Services
{
    public class UserService : IUserService
	{
		GtdContext context;

		public UserService(GtdContext context)
		{
			this.context = context;
		}

		public bool AuthenticateUser(string userName, string password)
		{
			var userRepo = new UserRepository(context);

			var user = userRepo.GetUser(userName);
			
			if (user == null)
				return false;

			return user.Authenticate(userName, password);
		}

		public bool RegisterUser(string userName, string password, string confirmPassword)
		{
			if (password != confirmPassword)
				return false;

			var userRepo = new UserRepository(context);
			var existingUser = userRepo.GetUser(userName);
			if (existingUser != null)
				return false;

			RNGCryptoServiceProvider rngCryptoSP = new RNGCryptoServiceProvider();
			byte[] saltArr = new byte[10];

			rngCryptoSP.GetBytes(saltArr);
			var salt = Convert.ToBase64String(saltArr);

			var user = User.Create(userName, password, salt);

			userRepo.Add(user);
			userRepo.SaveChanges();

			return true;
		}


		public User GetUser(string userName)
		{
			return (from u in context.Users
					where u.UserName == userName
					select u).FirstOrDefault();
		}
	}
}