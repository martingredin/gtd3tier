﻿using Gtd3Tier.DataAccessLayer.Context;
using Gtd3Tier.DataAccessLayer.Model;
using Shared.Repositories;
using System.Collections.Generic;

namespace Gtd3Tier.BusinessLayer.Services
{
    public class StateService : IStateService
	{
		GtdContext context;

		public StateService(GtdContext context)
		{
			this.context = context;
		}

		public KanbanState GetState(int stateId)
		{
			var stateRepo = new EFRepository<KanbanState>(context);
			return stateRepo.GetById(stateId);
		}

		public IEnumerable<KanbanState> GetAllStates()
		{
			var stateRepo = new EFRepository<KanbanState>(context);
			return stateRepo.List();
		}
	}
}