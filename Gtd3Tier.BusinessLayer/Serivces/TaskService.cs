﻿using Gtd.Infrastructure.Repositories;
using Gtd3Tier.DataAccessLayer.Context;
using Gtd3Tier.DataAccessLayer.Model;
using Shared.Repositories;
using System;
using System.Collections.Generic;

namespace Gtd3Tier.BusinessLayer.Services
{
    public class TaskService : ITaskService
	{
		GtdContext context;

		public TaskService(GtdContext context)
		{
			this.context = context;
		}

		public GtdTask AddTask(string userName, string name, string description, DateTime? startDate, DateTime? endDate)
		{
			var userRepo = new UserRepository(context);
			var stateRepo = new StateRepository(context);
			var taskRepo = new EFRepository<GtdTask>(context);

			var user = userRepo.GetUser(userName);

			var task = GtdTask.Create(name, description, startDate, endDate);
			task.KanbanStateId = stateRepo.GetStateByName("New").Id;

			context.SaveChanges();

			user.Tasks.Add(task);
			context.GtdTasks.Add(task);

			context.SaveChanges();

			return task;
		}

		public GtdTask UpdateTask(int taskId, string newName, string newDescription)
		{
			var taskRepo = new EFRepository<GtdTask>(context);
			var task = taskRepo.GetById(taskId);
			task.Name = newName;
			task.Description = newDescription;
			context.SaveChanges();
			return task;
		}

		public string MoveTaskToState(int taskId, Direction direction)
		{
			var taskRepo = new EFRepository<GtdTask>(context);
			var stateRepo = new EFRepository<KanbanState>(context);
			var task = taskRepo.GetById(taskId);
			var state = stateRepo.GetById((int)task.KanbanStateId);

			if (direction == Direction.Backwards && task.KanbanStateId != null)
				task.KanbanStateId = state.PreviousStateId;
			else if (direction == Direction.Forwards && task.KanbanStateId != null)
				task.KanbanStateId = state.NextStateId;

			context.SaveChanges();

			return stateRepo.GetById((int)task.KanbanStateId).Name;
		}

		public void ChangeTaskToState(int taskId, int stateId)
		{
			var stateRepo = new EFRepository<KanbanState>(context);
			var taskRepo = new EFRepository<GtdTask>(context);

			var newState = stateRepo.GetById(stateId);
			var task = taskRepo.GetById(taskId);

			task.KanbanStateId = newState.Id;
			context.SaveChanges();
		}

		public GtdTask GetTask(int taskId)
		{
			var taskRepo = new EFRepository<GtdTask>(context);
			return taskRepo.GetById(taskId);
		}

		public IEnumerable<GtdTask> GetAllTasks()
		{
			var taskRepo = new EFRepository<GtdTask>(context);
			return taskRepo.List();
		}

		public IEnumerable<GtdTask> GetTaskForUser(string userName)
		{
			var userRepo = new UserRepository(context);
			var taskRepo = new EFRepository<GtdTask>(context);

			var user = userRepo.GetUser(userName);
			return user.Tasks;
			
		}

		public int DeleteTask(int taskId)
		{
			var taskRepo = new EFRepository<GtdTask>(context);
			var task = taskRepo.GetById(taskId);
			taskRepo.Remove(task);
			context.SaveChanges();
			return taskId;
		}
	}
}