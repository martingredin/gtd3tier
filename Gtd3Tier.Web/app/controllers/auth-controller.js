﻿angular
    .module('GtdApp')
	.controller('AuthController', ['$scope', '$location', 'authService', 'dialogService', function ($scope, $location, authService, dialogService) {
		$scope.loggedIn = function ()
		{
			return authService.isAuthenticated();
		}

		$scope.loginData = {
			UserId: "",
			Password: ""
		};

		$scope.registrationData = new models.UserModel();

		$scope.login = function (userId, password) {
			authService
				.login(userId, password)
				.then(function (response) {
					$location.path('/main');
					$scope.loginData.UserId = "";
					$scope.loginData.Password = "";
				},
				function (err) {
					if (err) {
						dialogService.showDialog('Login error', err.error_description);
					} else {
						dialogService.showDialog('Login error', '');
					}
				});
			};

		$scope.logout = function () {
			authService.logOut();
			$location.path('/front');
		}

		$scope.register = function () {
			authService
				.saveRegistration($scope.registrationData)
				.then(function (response) {
					$('#authRegDialog').modal('hide');
					$scope.login($scope.registrationData.UserId, $scope.registrationData.Password);
				},
				function (err) {
					$('#authRegDialog').modal('hide');
					dialogService.showDialog('Registration error', "Failed to register user due to:" + err.error_description);
				});
		};

		$scope.clearRegForm = function () {
			$scope.registrationData = new models.UserModel();
		};
	}]);