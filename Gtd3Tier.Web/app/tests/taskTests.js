﻿/// <reference path="../../scripts/jasmine.js" />
/// <reference path="../../scripts/underscore.js" />
/// <reference path="../../scripts/angular.js" />
/// <reference path="../../scripts/angular-route.js" />
/// <reference path="../../scripts/angular-mocks.js" />
/// <reference path="../../scripts/angular-sanitize.js" />
/// <reference path="../../scripts/angular-local-storage.js" />
/// <reference path="../models.js" />
/// <reference path="../app.js" />
/// <reference path="../services/authentication.js" />
/// <reference path="../services/dialogs.js" />
/// <reference path="../services/tasks.js" />

describe('tasksTest', function () {
	//beforeEach(angular.mock.module('TestApp'));
	beforeEach(angular.mock.module('GtdApp'));

	var taskService, backend, cfg;

	beforeEach(angular.mock.inject(function ($httpBackend, _taskservice_, _cfg_) {
		taskService = _taskservice_;
		cfg = _cfg_;
		backend = $httpBackend;

		backend
			.expect("GET", cfg.getTasksUrl)
			.respond(
				[{ "id": 0, "name": "häpp", "description": "tjoho", "state": "New" },
				 { "id": 1, "name": "banzai", "description": "kamikaze", "state": "Analyzed" },
				 { "id": 2, "name": "down", "description": "less search for clean livi", "state": "Done" }]);
	}));

	it('get tasks, check no items', function () {
		expect(taskService.getTasks().length).toEqual(3);
	});
});