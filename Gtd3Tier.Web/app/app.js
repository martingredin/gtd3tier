﻿angular.module("GtdApp", ['ngRoute', 'ngSanitize', 'AuthenticationModule', 'LocalStorageModule', 'Dialogs', 'Tasks'])
	.constant('cfg', {
		loginUrl: "http://localhost:58887/token",
		regUrl: "http://localhost:58887/Api/User/Register",
		getTasksUrl: "http://localhost:58887/Api/Task/GetTasksByUser",
		createTaskUrl: "http://localhost:58887/Api/Task/CreateTask",
		editTaskUrl: "http://localhost:58887/Api/Task/EditTask",
		deleteTaskUrl: "http://localhost:58887/Api/Task/DeleteTask",
		moveTaskUrl: "http://localhost:58887/Api/Task/MoveTask",
		getAllStatesUrl: "http://localhost:58887/Api/State/GetAllStates"
	})
	.config(function ($routeProvider, $locationProvider, $httpProvider) {
		$routeProvider.when("/main", {
			templateUrl: "app/views/mainPage.html",
			css: "content/css/main.css"
		});

		$routeProvider.when("/front", {
			templateUrl: "app/views/frontPage.html",
			css: ""
		});

		$routeProvider.when("/stateMgmt", {
			templateUrl: "app/views/stateManagementPage.html",
			css: "content/css/stateManagement.css"
		});

		$routeProvider.otherwise({
			templateUrl: "app/views/frontPage.html",
			css: ""
		});

		$locationProvider.html5Mode(false).hashPrefix('!');

		$httpProvider.interceptors.push('authInterceptorService');
	})
	.controller('MainController', function($scope, $routeParams, $route, $location) {
		$scope.$watch(function () {
			return $route.current.css;
		},
		function (value) {
			$scope.css = value;
		});
	});
