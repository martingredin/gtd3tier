﻿angular
    .module('AuthenticationModule', [])
	.factory('authService', ['$http', '$q', 'localStorageService', 'cfg', function ($http, $q, localStorageService, cfg) {
		var authServiceFactory = {};

		authServiceFactory.authentication = {
			isAuth: false,
			userName: ""
		};

		authServiceFactory.saveRegistration =
			function (registrationData) {
				authServiceFactory.logOut();

				var deferred = $q.defer();
				$http
					.post(cfg.regUrl, registrationData)
					.success(function (response) {
						deferred.resolve(response);
					})
					.error(function (err, status) {
						deferred.reject(err);
					});

				return deferred.promise;
			};

		authServiceFactory.login =
			function (userId, password) {
				var data = "grant_type=password&username=" + userId + "&password=" + password;
				var deferred = $q.defer();
				$http
					.post(cfg.loginUrl, data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } })
					.success(function (response) {
						localStorageService.set('authorizationData', { token: response.access_token, userName: userId })
						authServiceFactory.authentication.isAuth = true;
						authServiceFactory.authentication.userName = userId;
						deferred.resolve(response);
					})
					.error(function (err, status) {
						authServiceFactory.logOut();
						deferred.reject(err);
					});

				return deferred.promise;
			};

		authServiceFactory.logOut =
			function () {
				localStorageService.remove('authorizationData');
				authServiceFactory.authentication.isAuth = false;
				authServiceFactory.authentication.userName = "";
			};

		authServiceFactory.fillAuthData =
			function () {
				var authData = localStorageService.get('authorizationData');
				if (authData) {
					authServiceFactory.authentication.isAuth = true;
					authServiceFactory.authentication.userName = authData.userName;
				}
			}

		authServiceFactory.isAuthenticated =
			function () {
				var authData = localStorageService.get('authorizationData');
				if (authData)
					return true;

				return false;
			}

		authServiceFactory.getUserName = 
			function () {
				var authData = localStorageService.get('authorizationData');
				if (authData) 
					return authData.userName;
				
				return "";
			}

		return authServiceFactory;
	}])
	.factory('authInterceptorService', ['$q', '$location', 'localStorageService', function ($q, $location, localStorageService) {
		var authInterceptorServiceFactory = {};

		authInterceptorServiceFactory.request = function (config) {
			config.headers = config.headers || {};
			var authData = localStorageService.get('authorizationData');
			if (authData)
				config.headers.Authorization = 'Bearer ' + authData.token;

			return config;
		}

		authInterceptorServiceFactory.responseError = function (rejection) {
			if (rejection.status === 401) {
				$location.path('/auth');
			}

			return $q.reject(rejection);
		};

		return authInterceptorServiceFactory;
	}]);