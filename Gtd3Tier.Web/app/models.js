﻿var models = {};

models.MoveTask = function (cons) {
	if (!cons) { cons = { }; }

	this.taskId = cons.taskId;
	this.direction = cons.direction;

	this.$merge = function (mergeObj) {
		if (!mergeObj) { mergeObj = { }; }
		this.taskId = mergeObj.taskId;
		this.direction = mergeObj.direction;
	}

	this.$equals = function (compareObj) {
		if (!compareObj) { return false; }
		if (this.taskId !== compareObj.taskId) { return false; };
		if (this.direction !== compareObj.direction) { return false; };
	return true;
	}
}



models.State = function (cons) {
	if (!cons) { cons = { }; }

	this.id = cons.id;
	this.name = cons.name;

	this.$merge = function (mergeObj) {
		if (!mergeObj) { mergeObj = { }; }
		this.id = mergeObj.id;
		this.name = mergeObj.name;
	}

	this.$equals = function (compareObj) {
		if (!compareObj) { return false; }
		if (this.id !== compareObj.id) { return false; };
		if (this.name !== compareObj.name) { return false; };
	return true;
	}
}



models.Task = function (cons) {
	if (!cons) { cons = { }; }

	this.id = cons.id;
	this.userName = cons.userName;
	this.taskName = cons.taskName;
	this.description = cons.description;
	this.startDate = cons.startDate;
	this.endDate = cons.endDate;

	this.$merge = function (mergeObj) {
		if (!mergeObj) { mergeObj = { }; }
		this.id = mergeObj.id;
		this.userName = mergeObj.userName;
		this.taskName = mergeObj.taskName;
		this.description = mergeObj.description;
		this.startDate = mergeObj.startDate;
		this.endDate = mergeObj.endDate;
	}

	this.$equals = function (compareObj) {
		if (!compareObj) { return false; }
		if (this.id !== compareObj.id) { return false; };
		if (this.userName !== compareObj.userName) { return false; };
		if (this.taskName !== compareObj.taskName) { return false; };
		if (this.description !== compareObj.description) { return false; };
		if (this.startDate !== compareObj.startDate) { return false; };
		if (this.endDate !== compareObj.endDate) { return false; };
	return true;
	}
}



models.TaskList = function (cons) {
	if (!cons) { cons = { }; }

	this.id = cons.id;
	this.name = cons.name;
	this.description = cons.description;
	this.state = cons.state;
	this.startDate = cons.startDate;
	this.endDate = cons.endDate;

	this.$merge = function (mergeObj) {
		if (!mergeObj) { mergeObj = { }; }
		this.id = mergeObj.id;
		this.name = mergeObj.name;
		this.description = mergeObj.description;
		this.state = mergeObj.state;
		this.startDate = mergeObj.startDate;
		this.endDate = mergeObj.endDate;
	}

	this.$equals = function (compareObj) {
		if (!compareObj) { return false; }
		if (this.id !== compareObj.id) { return false; };
		if (this.name !== compareObj.name) { return false; };
		if (this.description !== compareObj.description) { return false; };
		if (this.state !== compareObj.state) { return false; };
		if (this.startDate !== compareObj.startDate) { return false; };
		if (this.endDate !== compareObj.endDate) { return false; };
	return true;
	}
}



models.UserModel = function (cons) {
	if (!cons) { cons = { }; }

	this.userId = cons.userId;
	this.password = cons.password;
	this.confirmPassword = cons.confirmPassword;

	this.$merge = function (mergeObj) {
		if (!mergeObj) { mergeObj = { }; }
		this.userId = mergeObj.userId;
		this.password = mergeObj.password;
		this.confirmPassword = mergeObj.confirmPassword;
	}

	this.$equals = function (compareObj) {
		if (!compareObj) { return false; }
		if (this.userId !== compareObj.userId) { return false; };
		if (this.password !== compareObj.password) { return false; };
		if (this.confirmPassword !== compareObj.confirmPassword) { return false; };
	return true;
	}
}



