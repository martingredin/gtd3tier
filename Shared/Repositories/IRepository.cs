﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Repositories
{
	public interface IRepository<T>
	{
		T GetById(int id);
		void Add(T entity);
		void Remove(T entity);
		IEnumerable<T> List();
		void SaveChanges();
	}
}
