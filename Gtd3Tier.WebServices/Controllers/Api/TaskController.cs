﻿using Gtd.WebServices.Models;
using Gtd3Tier.BusinessLayer.Services;
using Gtd3Tier.DataAccessLayer.Context;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace Gtd.WebServices.Controllers.Api
{
    public class TaskController : ApiController
    {
		private ITaskService taskService;
		private IUserService userService;
		private IStateService stateService;

		public TaskController()
		{
			var gtdContext = new GtdContext();
			taskService = new TaskService(gtdContext);
			userService = new UserService(gtdContext);
			stateService = new StateService(gtdContext);
		}

		[HttpGet]
		[Authorize]
		[ActionName("GetTasksByUser")]
		public TaskListDto[] GetTasksByUser(string userName)
		{
			var user = userService.GetUser(userName);

			var dtos = new List<TaskListDto>();

			foreach (var task in user.Tasks)
			{
				var dto = new TaskListDto 
				{ 
					Id = task.Id,
					Name = task.Name, 
					Description = task.Description,
					StartDate = ((DateTime)task.StartDate).ToString("yyyy-MM-dd"),
					EndDate = ((DateTime)task.EndDate).ToString("yyyy-MM-dd")
				};

				dto.State = stateService.GetState((int)task.KanbanStateId).Name;

				dtos.Add(dto);
			}

			return dtos.ToArray();
		}

		[HttpPost]
		[Authorize]
		[ActionName("CreateTask")]
		public TaskListDto CreateTask(TaskDto dto)
		{
			DateTime? startDate = null;
			if (!string.IsNullOrEmpty(dto.StartDate))
				startDate = DateTime.Parse(dto.StartDate);

			DateTime? endDate = null;
			if (!string.IsNullOrEmpty(dto.EndDate))
				endDate = DateTime.Parse(dto.EndDate);

			var newTask = taskService.AddTask(dto.UserName, dto.TaskName, dto.Description, startDate, endDate);

			return new TaskListDto 
			{ 
				Id = newTask.Id, 
				Name = newTask.Name, 
				Description = newTask.Description, 
				State = stateService.GetState((int)newTask.KanbanStateId).Name,
				StartDate = newTask.StartDate != null ? ((DateTime)newTask.StartDate).ToString("yyyy-MM-dd") : "",
				EndDate = newTask.EndDate != null ? ((DateTime)newTask.EndDate).ToString("yyyy-MM-dd") : ""
			};
		}

		[HttpPost]
		[Authorize]
		[ActionName("EditTask")]
		public TaskListDto EditTask(TaskDto dto)
		{
			var updatedTask = taskService.UpdateTask(dto.Id, dto.TaskName, dto.Description);

			return new TaskListDto
			{
				Id = updatedTask.Id,
				Name = updatedTask.Name,
				Description = updatedTask.Description,
				State = stateService.GetState((int)updatedTask.KanbanStateId).Name,
				StartDate = updatedTask.StartDate != null ? ((DateTime)updatedTask.StartDate).ToString("yyyy-MM-dd") : "",
				EndDate = updatedTask.EndDate != null ? ((DateTime)updatedTask.EndDate).ToString("yyyy-MM-dd") : ""
			};
		}

		[HttpPost]
		[Authorize]
		[ActionName("DeleteTask")]
		public int DeleteTask([FromBody]int taskId)
		{
			int removedTaskId = taskService.DeleteTask(taskId);
			return removedTaskId;
		}

		[HttpPost]
		[Authorize]
		[ActionName("MoveTask")]
		public TaskListDto MoveTask(MoveTaskDto dto)
		{
			Direction d = (Direction)dto.Direction;
			var newStateName = taskService.MoveTaskToState(dto.TaskId, d);
			return new TaskListDto { Id = dto.TaskId, State = newStateName };
		}
    }
}