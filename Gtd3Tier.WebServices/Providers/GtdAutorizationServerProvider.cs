﻿using Gtd3Tier.BusinessLayer.Services;
using Gtd3Tier.DataAccessLayer.Context;
using Microsoft.Owin.Security.OAuth;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Gtd.WebServices.Providers
{
    public class GtdAutorizationServerProvider : OAuthAuthorizationServerProvider
	{
		public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext ctx)
		{
			ctx.Validated();
		}

		public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext ctx)
		{
			ctx.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

			IUserService userService = new UserService(new GtdContext());
			if (!userService.AuthenticateUser(ctx.UserName, ctx.Password))
			{
				ctx.SetError("invalid_grant", "The user name or password is incorrect.");
				return;
			}

			var identity = new ClaimsIdentity(ctx.Options.AuthenticationType);
			identity.AddClaim(new Claim("sub", ctx.UserName));
			identity.AddClaim(new Claim("role", "user"));

			ctx.Validated(identity);
		}
	}
}