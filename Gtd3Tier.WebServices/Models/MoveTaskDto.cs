﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gtd.WebServices.Models
{
	public class MoveTaskDto
	{
		public int TaskId { get; set; }
		public int Direction { get; set; }
	}
}