﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gtd.WebServices.Models
{
	public class UserModelDto
	{
		public string UserId { get; set; }
		public string Password { get; set; }
		public string ConfirmPassword { get; set; }
	}
}