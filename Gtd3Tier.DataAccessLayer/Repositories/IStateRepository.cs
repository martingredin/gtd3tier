﻿using Gtd3Tier.DataAccessLayer.Model;

namespace Gtd.Core.Repositories
{
    public interface IStateRepository
	{
		KanbanState GetStateByName(string name);
	}
}
