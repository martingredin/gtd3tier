﻿using Gtd.Core.Repositories;
using Gtd3Tier.DataAccessLayer.Context;
using Gtd3Tier.DataAccessLayer.Model;
using Shared.Repositories;
using System.Linq;

namespace Gtd.Infrastructure.Repositories
{
    public class StateRepository : EFRepository<KanbanState>, IStateRepository
	{
		protected GtdContext GtdContext
		{
			get { return (GtdContext)DataContext; }
		}

		public StateRepository(GtdContext context)
			: base(context)
		{

		}

		public KanbanState GetStateByName(string name)
		{
			return (from s in GtdContext.KanbaanStates
					where s.Name == name
					select s).FirstOrDefault();
		}
	}
}
