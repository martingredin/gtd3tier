﻿using Gtd3Tier.DataAccessLayer.Model;

namespace Gtd.Core.Repositories
{
    public interface IUserRepository
	{
		User GetUser(string userName);
	}
}
