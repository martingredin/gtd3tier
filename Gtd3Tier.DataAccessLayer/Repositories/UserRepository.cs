﻿using Gtd.Core.Repositories;
using Gtd3Tier.DataAccessLayer.Context;
using Gtd3Tier.DataAccessLayer.Model;
using Shared.Repositories;
using System.Linq;

namespace Gtd.Infrastructure.Repositories
{
    public class UserRepository : EFRepository<User>, IUserRepository
	{
		protected GtdContext GtdContext
		{
			get { return (GtdContext)DataContext; }
		}

		public UserRepository(GtdContext context)
			: base(context)
		{

		}

		public User GetUser(string userName)
		{
			return (from u in GtdContext.Users
					where u.UserName == userName
					select u).FirstOrDefault();
		}
	}
}
