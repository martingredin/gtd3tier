﻿using Shared.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gtd3Tier.DataAccessLayer.Model
{
	public class KanbanState : Entity<int>
	{
		public string Name { get; set; }
		public int? PreviousStateId { get; set; }
		public int? NextStateId { get; set; }
	}
}