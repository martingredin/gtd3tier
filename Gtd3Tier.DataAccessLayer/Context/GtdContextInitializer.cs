﻿using Gtd3Tier.DataAccessLayer.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Security.Cryptography;

namespace Gtd3Tier.DataAccessLayer.Context
{
    public class GtdContextInitializer : DropCreateDatabaseIfModelChanges<GtdContext>
	{
		KanbanState newState;
		KanbanState analyzedState;
		KanbanState inProgressState;
		KanbanState finishedState;

		private List<User> users = new List<User>();

		public GtdContextInitializer()
		{
			newState = new KanbanState {Name = "New" };
			analyzedState = new KanbanState {Name = "Analyzed" };
			inProgressState = new KanbanState {Name = "In progress" };
			finishedState = new KanbanState {Name = "Finished" };

			RNGCryptoServiceProvider rngCryptoSP = new RNGCryptoServiceProvider();
			byte[] saltArr = new byte[10];

			rngCryptoSP.GetBytes(saltArr);
			var salt1 = Convert.ToBase64String(saltArr);
			rngCryptoSP.GetBytes(saltArr);
			var salt2 = Convert.ToBase64String(saltArr);

			users.AddRange(new [] {
				User.Create("Martin", "abc123", salt1),
				User.Create("Kalle", "cde345", salt2)
			});
		}

		protected override void Seed(GtdContext context)
		{
			context.KanbaanStates.Add(newState);
			context.KanbaanStates.Add(analyzedState);
			context.KanbaanStates.Add(inProgressState);
			context.KanbaanStates.Add(finishedState);
			context.SaveChanges();

			newState.NextStateId = analyzedState.Id;
			analyzedState.PreviousStateId = newState.Id;
			analyzedState.NextStateId = inProgressState.Id;
			inProgressState.PreviousStateId = analyzedState.Id;
			inProgressState.NextStateId = finishedState.Id;
			finishedState.PreviousStateId = inProgressState.Id;

			context.Users.AddRange(users);
			
			context.SaveChanges();
			base.Seed(context);
		}
	}
}