﻿using Gtd3Tier.DataAccessLayer.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gtd3Tier.DataAccessLayer.Context
{
	public class GtdContext : DbContext
	{
		public virtual DbSet<User> Users { get; set; }
		public virtual DbSet<GtdTask> GtdTasks { get; set; }
		public virtual DbSet<KanbanState> KanbaanStates { get; set; }

		private const string dbName = "GtdDb";

		public GtdContext()
			: base(dbName)
		{
			Database.SetInitializer<GtdContext>(new GtdContextInitializer());
		}

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			

			//modelBuilder.Entity<KanbaanState>()
			//	.HasOptional(e => e.NextState)
			//	.WithMany()
			//	.HasForeignKey(m => m.NextStateId);

			//modelBuilder.Entity<KanbaanState>()
			//	.HasOptional(e => e.PreviousState)
			//	.WithMany()
			//	.HasForeignKey(m => m.PreviousStateId);

			//modelBuilder.Entity<KanbaanState>()
			//	.HasOptional(t => t.GtdTask)
			//	.WithRequired(s => s.KanbaanState);

			//modelBuilder.Entity<GtdTask>()
			//	.HasRequired(r => r.KanbaanState)
			//	.WithOptional(s => s.GtdTask);

			//modelBuilder.Entity<GtdTask>()
			//	.HasKey(k => k.Id)
			//	.Property(p => p.Id)
			//	.HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);



			modelBuilder.Entity<GtdTask>()
				.HasMany(m => m.SubTasks)
				.WithRequired(m => m.GtdTask);

			modelBuilder.Entity<User>()
				.HasMany(r => r.Tasks)
				.WithRequired(r => r.User);
				//.HasForeignKey(f => f.UserId);
		}
	}
}